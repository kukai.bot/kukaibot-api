import * as mysql from 'mysql2/promise'
import { CONFIG } from './interfaces/config'
import * as dotenv from 'dotenv'
import knex from 'knex'
process.env.TZ = 'Asia/Tokyo'

dotenv.config()
const config = (process.env as unknown) as CONFIG
const dbConfig = {
    host: config.DB_HOST,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    database: config.DB_DATABASE,
}
export const pool = async () => {
    const db = await mysql.createPool(dbConfig)
    return db
}
export const query = async (sql: string) => {
    const db = await pool()
    const [rows] = await db.query(sql)
    const results = rows as any
    db.end()
    return results
}
export const my = knex({ client: 'mysql' })