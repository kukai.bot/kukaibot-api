function nameVal(input: string, maxLength: number) {
    if (!input || input === '') return false
    if (input.length > maxLength) return false
    if (input.match(/([,'"./\\=?!;])/)) return false
    return true
}
export default nameVal