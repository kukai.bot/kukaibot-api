import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'

export const accesibleApi = async (request: STR2OBJ, prefix: string) => {
    const sql = my(`${prefix}_name`).select().where('User', request.i).toString()
    try {
        const result = await query(sql)
        if (!result[0]['Name']) return false
        return true
    } catch (err) {
        return false
    }
}
export const securityCheck = async (prefix: string, i: string, at: string) => {
    const sql = my(`${prefix}_login`).select().where('User', i).where('Code', at).toString()
    try {
        const result = await query(sql)
        if (!result[0]['Code']) return false
        return true
    } catch (err) {
        return false
    }
}
export const securityCheckOnline = async (prefix: string, token: string) => {
    const sql = my(`${prefix}_login`).select().where('Code', token).toString()
    try {
        const result = await query(sql)
        if (result[0]['User']) return result[0]['User']
        return null
    } catch (err) {
        return null
    }
}