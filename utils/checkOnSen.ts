import { query, my } from '../mysql'

/**
 * Returns 'status' of your kukai about OnSen
 *
 *
 * @param prefix - Group prefix: string
 * @param id - ID of kukai: number
 * @returns -1: not closed yet, 0: OnSen is disabled, 1: please select(Sen), 2: can check the result
 *
 */
export default async function (prefix: string, id: number) {
    const sql = my(`${prefix}_kukai`).select().where('ID', id).toString()
    try {
        const result = await query(sql)
        if (result[0]['PDF'] !== 3) {
            return -1
        } else if (result[0]['OnSen'] === 0) {
            return 0
        } else if (result[0]['OnSenResult'] === 0) {
            return 1
        } else {
            return 2
        }
    } catch (err) {
        return -2
    }
}