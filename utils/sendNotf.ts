import fetch from 'node-fetch'
import axios from 'axios'
import { query, my} from '../mysql'
import { kukaiCount } from '../utils'

export default async (message: string, title: string, template: boolean, db: any, group: string, type: string, id: number | null) => {
    let sql = my(`${group}_notf`).select().where(type, 1).toString()
    if(type === 'all') sql = my(`${group}_notf`).select().toString()
    let result
    try {
        result = await query(sql) as any
    } catch (err) {
        return false
    }
    for(const user of result) {
        const token = user.Token
        const i = user.User
        if(template && id) {
            const ctYou = await kukaiCount.getKukaiCountByUser(id, i, group, db)
            message = message.replace(/%d/, `${ctYou}`)
        }
        await axios.post(`https://expo.io/--/api/v2/push/send`, {
            to: token,
            title,
            body: message
        })
    }

    return { success: true }
}