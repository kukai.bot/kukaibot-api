import AWS from 'aws-sdk'
import fs from 'fs'
import moment from 'moment'
import { CONFIG } from '../interfaces/config'
import * as dotenv from 'dotenv'
dotenv.config()
const config = (process.env as unknown) as CONFIG

export default async function main(id: number, revealed: boolean, prefix: string) {
    if(prefix === 'rn') prefix = ''
    const target = `https://drciwpocap67s.cloudfront.net/${prefix}kukai${id}.${revealed ? 'revealed.' : ''}pdf`
    const expires = moment.utc().add(1, 'days').unix()
    const privKey = fs.readFileSync(config.PEM_AT).toString().replace(/\\n/g, '\n')
    console.log('use this key')
    console.log(privKey)
    const url = await getSignedUrlAsync(
        config.KEYPAIR_ID,
        privKey,
        {
            url: target,
            expires
        }
    )
    return url
}
function getSignedUrlAsync(keypairId: string, privateKey: string, options: any) {
    return new Promise((resolve, reject) => {
        // Signerインスタンスを生成
        const signer = new AWS.CloudFront.Signer(keypairId, privateKey)
        // URL生成
        signer.getSignedUrl(options, (err, url) => {
            if (err) {
                reject(err)
            }
            resolve(url)
        })
    })
}