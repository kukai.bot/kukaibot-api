import { query, my, pool } from '../mysql'

export const id = async function (group: string, stage: number, kukai: string) {
    const sqlB = my(`${group}_room`).update({
        Stage: stage
    }).where('Kukai', kukai).toString()
    try {
        await query(sqlB)
    } catch (err) {
        return false
    }
    return {
        success: true
    }
}
export const code = async function (group: string, stage: number, code: string) {
    const sqlB = my(`${group}_room`).update({
        Stage: stage
    }).where('Code', code).toString()
    try {
        await query(sqlB)
    } catch (err) {
        return false
    }
    return {
        success: true
    }
}