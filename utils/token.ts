import crypto from 'crypto'
export default function token(N: number) {
    return crypto.randomBytes(N).toString('base64').replace(/\+/g, '').substring(0, N)
}