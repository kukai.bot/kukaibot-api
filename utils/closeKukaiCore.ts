import { CONFIG } from '../interfaces/config'
import * as dotenv from 'dotenv'
import { query, my } from '../mysql'

dotenv.config()
const config = (process.env as unknown) as CONFIG
import axios from 'axios'
import fetch from 'node-fetch'
import { sendNotf } from '../utils'
export const core =  async (id: number, prefix: string) => {
    const sql = my(`${prefix}_kukai`).select().where('ID', id).toString()
    let result
    try {
        result = await query(sql)
        if(!result) return false
        if(result.length !== 1) return false
    } catch (err) {
        return false
    }
    await sendNotf(`句会${result[0].Name} が締め切られました`, `締切`,  false, null, prefix, 'KukaiEndNotf', null)
    await pdf(id, prefix)
    return true
}
export const pdf = async (id: number, prefix: string) => {
    const sqlB = my(`${prefix}_kukai`).update({
        PDF: 1
    }).where('ID', id).toString()
    try {
        await query(sqlB)
    } catch (err) {
        return false
    }
    const options = {
        timeout: 100000
    }
    const target = `https://${config.PDF_HODT}/pdf/${prefix}/${id}?pwd=${config.PDF_PASSWORD}`
    console.log(target)
    console.error('kukai make pdf go', new Date().toLocaleString())
    const sqlA = my(`${prefix}_kukai`).update({
        PDF: 3
    }).where('ID', id).toString()
    try {
        await fetch(target, options)
        console.error('kukai make pdf fetch completed')
        await query(sqlA)
        console.error('kukai make pdf sql completed')
    } catch (err) {
        console.error('kukai make pdf error', err)
        return false
    }
}