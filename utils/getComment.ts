import { query, my } from '../mysql'

export default async function (prefix: string, id: number) {
    const sql = my(`${prefix}_kukai`).select().where('ID', id).toString()
    try {
        const result = await query(sql)
        if (result[0]['OnSenComment']) {
            return result[0]['OnSenComment']
        } else {
            return null
        }
    } catch (err) {
        return null
    }
}