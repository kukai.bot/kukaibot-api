function errorHandle(str: string) {
    return {
        error: true,
        status: str
    }
}
export default errorHandle