import { query, my } from '../mysql'

export default async function (prefix: string, id: number) {
    const sql = my(`${prefix}_kukai`).select('OnSenZero').where('ID', id).toString()
    try {
        const result = await query(sql)
        return parseInt(result[0]['OnSenZero'], 10) ? true : false
    } catch (err) {
        return false
    }
}