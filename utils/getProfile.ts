import fetch from 'node-fetch'
import { CONFIG } from '../interfaces/config'
import * as dotenv from 'dotenv'
import { query, my } from '../mysql'

dotenv.config()
const config = (process.env as unknown) as CONFIG

export const line = async (userId: string) => {
    const f = await data(userId)
    return f
}
const data = async (userId: string) => {
    const target = `https://api.line.me/v2/bot/profile/${userId}`
    const response = await fetch(target, {
        headers: {
            'Content-Type': `application/json; charser=UTF-8`,
            'Authorization': `Bearer ${config.LINE_AUTH}`
        }
    })
    if (response.ok) return response.json()
    return
}

export const idToName = async (prefix: string, i: string) => {
    const sql = my(`${prefix}_name`).select('Name').where('User', i).toString()
    try {
        const result = await query(sql)
        return result[0]['Name']
    } catch (err) {
        return null
    }
}