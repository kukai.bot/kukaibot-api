import { query, my } from '../mysql'

export default async (prefix: string, id: number) => {
    const sql = my(`${prefix}_kukai`).select('User').where('ID', id).toString()
    try {
        const result = await query(sql)
        return result[0]['User']
    } catch (err) {
        return null
    }
}