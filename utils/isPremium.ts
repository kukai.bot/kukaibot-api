import { query, my } from '../mysql'

export default async function isPremium(prefix: string) {
    const sql = my(`${prefix}_config`).select('Value').where('Name', 'premium').toString()
    try {
        const result = await query(sql)
        return parseInt(result[0]['Value'], 10) ? true : false
    } catch (err) {
        return false
    }
}