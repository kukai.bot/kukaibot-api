import { query, my } from '../mysql'

export async function admin(prefix: string, userId: string) {
    const sql = my(`${prefix}_name`).select('Admin').where('User', userId).toString()
    try {
        const result = await query(sql)
        return result[0]['Admin']
    } catch (err) {
        return false
    }
}
export async function dev(prefix: string, userId: string) {
    const sql = my(`${prefix}_name`).select('Dev').where('User', userId).toString()
    try {
        const result = await query(sql)
        return result[0]['Dev']
    } catch (err) {
        return false
    }
}