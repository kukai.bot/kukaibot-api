import axios from 'axios'
import { query, my } from '../mysql'
import { errorHandle } from '../utils'

export default async function (code: string) {
    const sqlu = my(`kukaibotLogin`).select().where('Code', code).toString()
    let i
    try {
        const result = await query(sqlu) as any
        if (!result.length) throw errorHandle('cannot use this code')
        i = result[0]['User']
        const sqld = my(`kukaibotLogin`).delete().where('Code', code).toString()
        await query(sqld)
        const prov = i.split(':')[0]
        if (prov === 'google') {
            const url = `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${i.split(':')[1]}`
            const profile = await axios.get(url)
            const { id } = profile.data
            return `${prov}:${id}`
        } else if (prov === 'apple') {
            return i
        }

    } catch (err) {
        return false
    }
}