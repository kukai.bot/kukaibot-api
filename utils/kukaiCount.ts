import { pool, my } from '../mysql'
export const getKukaiCount = async (id: number, prefix: string, db: any) => {
    const sql = my(`${prefix}_haiku`).select('ID').where('Kukai', id).toString()
    try {
        const [result] = await db.query(sql)
        return result.length as number
    } catch (err) {
        return false
    }
}

export const getKukaiCountByUser = async (id: number, user: string, prefix: string, db: any) => {
    const sql = my(`${prefix}_haiku`).select('ID').where('Kukai', id).where('User', user).toString()
    try {
        const [result] = await db.query(sql)
        return result.length as number
    } catch (err) {
        return false
    }
}