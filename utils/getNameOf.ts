import { query, my } from '../mysql'

export const group = async (prefix: string) => {
    const sql = my(`${prefix}_config`).select('Value').where('Name', 'name').toString()
    try {
        const result = await query(sql)
        return result[0]['Value']
    } catch (err) {
        return false
    }
}
export const kukai = async (prefix: string, id: number) => {
    const sql = my(`${prefix}_kukai`).select('Name').where('ID', id).toString()
    try {
        const result = await query(sql)
        return result[0]['Name']
    } catch (err) {
        return null
    }
}