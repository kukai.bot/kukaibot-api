import { query, my } from '../mysql'

export default async function getInvitationCode(prefix: string) {
    const sql = my(`${prefix}_config`).select('Value').where('Name', 'phrase').toString()
    try {
        const result = await query(sql)
        return result[0]['Value']
    } catch (err) {
        return false
    }
}