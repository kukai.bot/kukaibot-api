import Koa from 'koa'
import Router from 'koa-router'
import koaBody from 'koa-body'
import fs from 'fs'
import * as api from './api'
import kusaku from './kusaku'
import * as login from './login/index'
import cors from '@koa/cors'
import { CONFIG } from './interfaces/config'
import * as dotenv from 'dotenv'
import { changeKusakuStage, getComment, getNameOf } from './utils'
dotenv.config()
const config = (process.env as unknown) as CONFIG

const router = new Router()
const koa = new Koa()
koa.use(koaBody())
koa.use(cors())

router.post('/v1/join', async (ctx, next) => {
    const body = await api.v1Join(ctx.request.body)
    ctx.body = body
})
router.post('/v1/continue', async (ctx, next) => {
    const body = await api.v1Continue(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kukais', async (ctx, next) => {
    const body = await api.v1Kukais(ctx.request.body)
    ctx.body = body
})
router.post('/v1/make_kukai', async (ctx, next) => {
    const body = await api.v1MakeKukai(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kukai/:id/delete', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1DeleteKukai(ctx.request.body, parseInt(id, 10))
    ctx.body = body
})
router.post('/v1/kukai/:id/close', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1CloseKukai(ctx.request.body, parseInt(id, 10))
    ctx.body = body
})
router.post('/v1/kukai/:id/post', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1PostKukai(ctx.request.body, parseInt(id, 10))
    ctx.body = body
})
router.post('/v1/kukai/:id/my_haiku', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1GetHaiku(ctx.request.body, parseInt(id, 10), true)
    ctx.body = body
})
router.post('/v1/kukai/:id/haiku', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1GetHaiku(ctx.request.body, parseInt(id, 10), false)
    ctx.body = body
})
router.post('/v1/kukai/:id/edit', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1EditHaiku(ctx.request.body, parseInt(id, 10))
    ctx.body = body
})
router.post('/v1/kukai/:id/get_pdf', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1GetPDF(ctx.request.body, parseInt(id, 10))
    ctx.body = body
})
router.post('/v1/members', async (ctx, next) => {
    const body = await api.v1Members(ctx.request.body)
    ctx.body = body
})
router.post('/v1/member/:id/delete', async (ctx, next) => {
    const id = ctx.params.id
    const body = await api.v1DeleteMember(ctx.request.body, id)
    ctx.body = body
})
router.post('/v1/member/:id/admin/:action', async (ctx, next) => {
    const { id, action } = ctx.params
    const body = await api.v1AdminControl(ctx.request.body, id, action)
    ctx.body = body
})
router.post('/v1/settings', async (ctx, next) => {
    const body = await api.v1Settings(ctx.request.body)
    ctx.body = body
})
router.post('/v1/setting/invitation_code', async (ctx, next) => {
    const body = await api.v1InvitCodeUpdate(ctx.request.body)
    ctx.body = body
})
router.post('/v1/setting/notification', async (ctx, next) => {
    const body = await api.v1NotifUpdate(ctx.request.body)
    ctx.body = body
})
router.post('/v1/setting/haigo', async (ctx, next) => {
    const body = await api.v1HaigoUpdate(ctx.request.body)
    ctx.body = body
})
router.post('/v1/setting/web', async (ctx, next) => {
    const body = await api.v1WebSetting(ctx.request.body)
    ctx.body = body
})
router.post('/v1/connect', async (ctx, next) => {
    const body = await api.v1Connect(ctx.request.body)
    ctx.body = body
})
router.get('/v1/cron', async (ctx, next) => {
    const body = await api.v1Cron()
    ctx.body = body
})
router.post('/v1/new', async (ctx, next) => {
    const body = await api.v1New(ctx.request.body)
    ctx.body = body
})
router.post('/v1/online/create', async (ctx, next) => {
    ctx.body = await api.v1OnlineCreate(ctx.request.body)
})
router.post('/v1/online/status', async (ctx, next) => {
    ctx.body = await api.v1OnlineStatus(ctx.request.body)
})
router.post('/v1/online/close', async (ctx, next) => {
    ctx.body = await api.v1OnlineClose(ctx.request.body)
})
router.post('/v1/online/estimate', async (ctx, next) => {
    ctx.body = await api.v1OnlineEstimate(ctx.request.body)
})
router.post('/v1/online/result', async (ctx, next) => {
    ctx.body = await api.v1OnlineResult(ctx.request.body)
})
router.post('/v1/online/select', async (ctx, next) => {
    ctx.body = await api.v1OnlineSelect(ctx.request.body)
})

router.get('/', async (ctx, next) => {
    ctx.body = fs.readFileSync('./static/index.html').toString()
})
router.get('/login', async (ctx, next) => {
    ctx.body = fs.readFileSync('./static/login.html').toString()
})
router.get('/new', async (ctx, next) => {
    ctx.body = fs.readFileSync('./static/new.html').toString().replace(/%pay%/, config.PAYJP_PUBLIC)
})
router.get('/edit', async (ctx, next) => {
    ctx.body = fs.readFileSync('./static/edit.html').toString().replace(/%pay%/, config.PAYJP_PUBLIC)
})
router.get('/login/google', async (ctx, next) => {
    const { from } = ctx.request.query
    const url = login.google.loginRedirect(from)
    ctx.redirect(url)
})
router.get('/login/google/redirect', async (ctx, next) => {
    const { code, state } = ctx.request.query
    const id = await login.google.getToken(code)
    const token = await login.writeId('google', id)
    if (!token) ctx.redirect('/')
    if (state === 'app') {
        ctx.redirect(`https://kukaibot-web.0px.io/login?code=${token}`)
    } else if (state === 'new') {
        ctx.redirect(`https://kukaibot.0px.io/new?code=${token}`)
    } else if (state.split(':')[0] === 'edit') {
        ctx.redirect(`https://kukaibot.0px.io/edit?code=${token}&group=${state.split(':')[1]}`)
    }
})
router.post('/login/apple/redirect', async (ctx, next) => {
    const { id_token, state } = ctx.request.body
    const id = await login.apple.getToken(id_token)
    const token = await login.writeId('apple', id)
    if (!token) ctx.redirect('/')
    if (state === 'app') {
        ctx.redirect(`https://kukaibot-web.0px.io/login?code=${token}`)
    } else if (state === 'new') {
        ctx.redirect(`https://kukaibot.0px.io/new?code=${token}`)
    } else if (state.split(':')[0] === 'edit') {
        ctx.redirect(`https://kukaibot.0px.io/edit?code=${token}&group=${state.split(':')[1]}`)
    }
})
router.get('/online/:gr/:id/new', async (ctx, next) => {
    const { gr, id } = ctx.params
    ctx.body = fs.readFileSync('./static/onlineNew.html').toString()
        .replace(/%name%/, await getNameOf.kukai(gr, id))
        .replace(/%gr%/, gr)
        .replace(/%id%/, id)
})
router.get('/online/:gr/:id/menu', async (ctx, next) => {
    const { gr, id } = ctx.params
    ctx.body = fs.readFileSync('./static/online.html').toString()
        .replace(/%name%/, await getNameOf.kukai(gr, id))
        .replace(/%gr%/g, gr)
        .replace(/%id%/g, id)
})
router.get('/online/:gr/:id/select', async (ctx, next) => {
    const { gr, id } = ctx.params
    ctx.body = fs.readFileSync('./static/select.html').toString()
        .replace(/%name%/, await getNameOf.kukai(gr, id))
        .replace(/%gr%/g, gr)
        .replace(/%id%/g, id)
})
router.get('/online/:gr/:id/result', async (ctx, next) => {
    const { gr, id } = ctx.params
    ctx.body = fs.readFileSync('./static/result.html').toString()
        .replace(/%name%/, await getNameOf.kukai(gr, id))
        .replace(/%gr%/g, gr)
        .replace(/%id%/g, id)
})
router.get('/static/:type/:asset', async (ctx, next) => {
    const { asset, type } = ctx.params
    let mime = ''
    if (type === 'css') mime = 'text/css'
    ctx.set('Content-Type', mime)
    ctx.body = fs.readFileSync(`./static/${asset}`).toString()
})
koa.use(kusaku.routes())
koa.use(kusaku.allowedMethods())
koa.use(router.routes())
koa.use(router.allowedMethods())
koa.listen(8000, () => {
    console.log('Server started!!')
})