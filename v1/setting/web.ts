import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, isAdmin, isPremium, nameVal, tempLogin } from '../../utils'
import { CONFIG } from '../../interfaces/config'
import * as dotenv from 'dotenv'
import axios from 'axios'
dotenv.config()
const config = (process.env as unknown) as CONFIG

export default async function (request: STR2OBJ) {
    const { group, token, code, name } = request
    const i = await tempLogin(code)
    if (typeof i !== 'string') {
        return errorHandle(JSON.stringify(i))
    }
    if (!nameVal(name, 20)) return errorHandle('cannot use this name')

    if (!await isAdmin(group, i)) return errorHandle('you are not admin')

    const sql2 = my(`${group}_config`).update({
        Value: name
    }).where('Name', 'name').toString()
    try {
        await query(sql2)
    } catch (err) {
        return errorHandle(`cannot update name`)
    }
    if (!await isPremium(group) && token) {
        let p
        if (token) {
            try {
                p = await axios.post(`https://api.pay.jp/v1/charges`, `amount=500&currency=jpy&card=${token}`, {
                    auth: {
                        username: config.PAYJP_SECRET,
                        password: ''
                    }
                })
                if(!p.data.paid) return errorHandle(p.data.failure_message)
                const sql3 = my(`${group}_config`).update({
                    Value: 1
                }).where('Name', 'premium').toString()
                try {
                    await query(sql3)
                } catch (err) {
                    return errorHandle(`【重大なエラー】お支払いは完了しましたが、データベースの書き込みに失敗しています。速やかに開発者にご連絡ください。`)
                }
                return { success: true }
            } catch (e) {
                let message = e.response.data.error.message
                if(!message) message = e.response.data.failure_message
                return errorHandle(message)
            }
        }
    } else {
        return { success: true }
    }
}