import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, isAdmin } from '../../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at, code } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if (!await isAdmin(group, i)) return errorHandle('you are not admin')

    const sql2 = my(`${group}_config`).update({
        Value: code
    }).where('Name', 'phrase').toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot update invitation_code`)
    }
}