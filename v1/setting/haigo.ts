import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, isAdmin } from '../../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at, haigo } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const sql2 = my(`${group}_name`).update({
        Name: haigo
    }).where('User', i).toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot update haigo`)
    }
}