import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure } from '../../utils'
interface Q {
    OneDayNotf?: 0 | 1
    TenMinsNotf?: 0 | 1
    NewKukaiNotf?: 0 | 1
    KukaiEndNotf?: 0 | 1
    Token?: string
    User?: string
}

export default async function (request: STR2OBJ) {
    const { group, i, at, target, value, notificationId } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let q = {} as Q
    let del = false
    if(target == 'oneDay') {
        q.OneDayNotf = value ? 1 : 0
    }
    if(target == 'tenMins') {
        q.TenMinsNotf = value ? 1 : 0
    }
    if(target == 'newKukai') {
        q.NewKukaiNotf = value ? 1 : 0
    }
    if(target == 'kukaiEnd') {
        q.KukaiEndNotf = value ? 1 : 0
    }
    if(target == 'notificationId') {
        q.User = i
        q.Token = value
        if(value == 'delete') del = true
    }
    let sql = my(`${group}_notf`).insert(q).toString()
    if(notificationId) sql = my(`${group}_notf`).update(q).where('User', i).where('Token', notificationId).toString()
    if(del) sql = my(`${group}_notf`).delete().where('User', i).where('Token', notificationId).toString()
    try {
        await query(sql)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot update invitation_code`)
    }
}