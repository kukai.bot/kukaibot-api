import { STR2OBJ } from "../../interfaces/common"
import moment from 'moment-timezone'
import axios from 'axios'
import kigoRaw from '../../odai/kigo'
import jidaiRaw from '../../odai/kanji'
moment.tz.setDefault("Asia/Tokyo")
const getRandom = 'https://commons.wikimedia.org/wiki/Special:Random/File'
export default async function (mode: STR2OBJ) {
    const { kigo, jidai, gazou } = mode
    let ret = []
    if(kigo === 'true') ret.push(getListOfKigo())
    if(jidai === 'true') ret.push(shuffle(jidaiRaw)[0])
    if (gazou === 'true') {
        const data = await axios.get(getRandom)
        const red = data.request.res.responseUrl
        const fileUrl = red.replace('File:', 'Special:FilePath/')
        const data2 = await axios.get(fileUrl)
        const red2 = data2.request.res.responseUrl
        return { type: 'image', content: red2 }
    }

    const img = { type: 'image', content: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Amsterdam_Canals_-_July_2006.jpg/1280px-Amsterdam_Canals_-_July_2006.jpg' }
    const text = { type: 'text', content: 'aaa' }
    return { type: 'text', content: shuffle(ret)[0] }
}
const shuffle = ([...array]) => {
    for (let i = array.length - 1; i >= 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

const getListOfKigo = () => {
    const kigo = kigoRaw as any
    const month = moment().format('M')
    const day = moment().format('D')
    const year = moment().format('YYYY')
    const feb = parseInt(year, 10) % 4 === 0 ? 29 : 28
    let season = ''
    let dayFromNewYear = 0
    if (month === '1') {
        dayFromNewYear = parseInt(day, 10)
    } else if (month === '2') {
        dayFromNewYear = parseInt(day, 10) + 31
    } else if (month === '3') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb
    } else if (month === '4') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31
    } else if (month === '5') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30
    } else if (month === '6') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31
    } else if (month === '7') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30
    } else if (month === '8') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30 + 31
    } else if (month === '9') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30 + 31 + 31
    } else if (month === '10') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30
    } else if (month === '11') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31
    } else if (month === '12') {
        dayFromNewYear = parseInt(day, 10) + 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
    }
    let ref = ''
    if (dayFromNewYear < 5) {
        ref = 'mainWinter'
        season = 'newYear'
    } else if (dayFromNewYear < 35) {
        ref = 'endWinter'
        season = 'entireWinter'
    } else if (dayFromNewYear < 31 + feb + 6) {
        ref = 'firstSpring'
        season = 'entireSpring'
    } else if (dayFromNewYear < 31 + feb + 31 + 6) {
        ref = 'mainSpring'
        season = 'entireSpring'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 6) {
        ref = 'endSpring'
        season = 'entireSpring'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 6) {
        ref = 'firstSummer'
        season = 'entireSummer'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 7) {
        ref = 'mainSummer'
        season = 'entireSummer'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 31 + 8) {
        ref = 'endSummer'
        season = 'entireSummer'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 8) {
        ref = 'firstAutumn'
        season = 'entireAutumn'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 8) {
        ref = 'mainAutumn'
        season = 'entireAutumn'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 8) {
        ref = 'endAutumn'
        season = 'entireAutumn'
    } else if (dayFromNewYear < 31 + feb + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 8) {
        ref = 'firstWinter'
        season = 'entireWinter'
    } else {
        ref = 'mainWinter'
        season = 'entireWinter'
    }
    let ret = [...kigo[season], ...kigo[ref]]
    return shuffle(ret)[0]
}