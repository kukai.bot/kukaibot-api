import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure } from '../../utils'
import moment from 'moment-timezone'
moment.tz.setDefault("Asia/Tokyo")

export const myStatus = async function (request: STR2OBJ) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let sql = my(`${group}_kusaku`).select().where('User', i).orderBy('CreatedAt', 'desc').toString()
    let result
    try {
        result = await query(sql)
    } catch (err) {
        console.error(err)
        return errorHandle(`cannot find haiku`)
    }
    const oneWeekAgo = moment().add(-1, 'week')
    const oneMonthAgo = moment().add(-1, 'month')
    let solo = 0
    let together = 0
    let thisWeek = 0
    let thisMonth = 0
    let total = 0
    for (let ku of result) {
        if(ku.Solo) solo++
        if(!ku.Solo) together++
        const make = moment(new Date(ku.CreatedAt))
        if(make.diff(oneWeekAgo) > 0) thisWeek++
        if(make.diff(oneMonthAgo) > 0) thisMonth++
        total++
    }
    return {
        success: true,
        solo,
        together,
        thisWeek,
        thisMonth,
        total
    }
}

export const allStatus = async function (request: STR2OBJ) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let sql = my(`${group}_kusaku`).select().orderBy('CreatedAt', 'desc').toString()
    let result
    try {
        result = await query(sql)
    } catch (err) {
        console.error(err)
        return errorHandle(`cannot find haiku`)
    }
    const oneWeekAgo = moment().add(-1, 'week')
    const oneMonthAgo = moment().add(-1, 'month')
    let solo = 0
    let together = 0
    let thisWeek = 0
    let thisMonth = 0
    let total = 0
    for (let ku of result) {
        if(ku.Solo) solo++
        if(!ku.Solo) together++
        const make = moment(new Date(ku.CreatedAt))
        if(make.diff(oneWeekAgo) > 0) thisWeek++
        if(make.diff(oneMonthAgo) > 0) thisMonth++
        total++
    }
    return {
        success: true,
        solo,
        together,
        thisWeek,
        thisMonth,
        total
    }
}