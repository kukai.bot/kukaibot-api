import { query, my } from '../../mysql'
import { errorHandle, secure, token as genToken, nameVal } from '../../utils'
interface SoloKusaku {
    haiku: string
    room: string
    i: string
    group: string
    at: string
}

export default async function (request: SoloKusaku) {
    const { group, i, at, haiku, room } = request
    console.log(request)
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let error = false
    let errorMeg = ''
    const sql2 = my(`${group}_kusaku`).insert({
        User: i,
        Haiku: haiku,
        Room: room,
        Solo: 1
    }).toString()
    if (!nameVal(haiku, 34)) {
        error = true
        errorMeg = `this haiku ${haiku} is not valid(maybe too long)`
    } else {
        try {
            await query(sql2)
        } catch (err) {
        }
    }
    if (!error) return { success: true }
    return errorHandle(errorMeg)
}