import { query, my, pool } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { Room } from '../../interfaces/kusaku'
import { isAdmin, isDev, errorHandle, kukaiCount, secure } from '../../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let limit = 40
    const limitParam = parseInt(request.limit ? request.limit : '0', 10)
    if (limitParam <= 40 && limitParam >= 1) limit = limitParam
    const sql = my(`${group}_room`).select().limit(limit).orderBy('CreatedAt', 'desc').toString()
    let result: Room[]
    try {
        result = await query(sql)
    } catch (err) {
        console.log(err)
        return errorHandle(`cannot get any room`)
    }
    let admin = false
    const ret = []
    const dev = await isDev(group, i)
    const db = await pool()
    if (await isAdmin(group, i)) admin = true
    for (const room of result) {
        const name = room['Name']
        if (name.match(/test:.+/) && !dev) continue
        if (room.Stage === 3) continue
        const flagStr = room.Odai
        const flagArr = flagStr.split(',')
        const json = {
            name,
            code: room.Code,
            id: room.ID,
            owner: room.User,
            time: room.Time,
            set: room.Set,
            nowSet: room.NowSet,
            stage: room.Stage,
            kukaiId: room.Kukai,
            createdAt: room.CreatedAt,
            member: room.Member,
            odai: {
                kigo: flagArr[0] === '1' ? true : false,
                jidai: flagArr[1] === '1' ? true : false,
                gazou: flagArr[2] === '1' ? true : false
            }
        }
        ret.push(json)
    }
    db.end()
    return {
        success: true,
        rooms: ret
    }
}
