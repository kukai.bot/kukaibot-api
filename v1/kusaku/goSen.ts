import { query, my } from '../../mysql'
import { KusakuNew } from '../../interfaces/kusaku'
import { errorHandle, secure, token as genToken, nameVal } from '../../utils'

export default async function (request: any) {
    const { room, name, group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const sql2 = my(`${group}_kukai`).insert({
        User: i,
        Name: name,
        PDF: 3,
        Deadline: null,
        OnSenComment: 'これは句作ルームでできた句の選句です。',
        OnSen: 1,
        OnSenZero: 0,
        IsKusaku: 1
    }).toString()
    let makeKukai
    try {
        makeKukai = await query(sql2)
    } catch (err) {
        console.log(err)
        return errorHandle(`cannot create OnSen`)
    }

    const sql3 = my(`${group}_kukai`).select().where('Name', name).orderBy('CreatedUTC', 'desc').toString()
    let getKukai
    try {
        [getKukai] = await query(sql3)
    } catch (err) {
        console.log(err)
        return errorHandle(`cannot create OnSen`)
    }

    const sql = my(`${group}_kusaku`).select().where('Room', room).toString()
    let result
    try {
        result = await query(sql) as KusakuNew[]
    } catch (err) {
        return errorHandle(`cannot find kukai`)
    }
    let ret = []
    for (let ku of result) {
        const code = genToken(5)
        ret.push({
            User: ku.User,
            Haiku: ku.Haiku,
            Kukai: getKukai.ID,
            Code: code
        })
    }
    const sql1 = my(`${group}_haiku`).insert(ret).toString()
    try {
        await query(sql1)
    } catch (err) {
        console.error(err)
        return errorHandle(`internal error`)
    }

    const sqlB = my(`${group}_room`).update({
        Kukai: getKukai.ID
    }).where('Code', room).toString()
    try {
        await query(sqlB)
    } catch (err) {
        return false
    }

    return { success: true, id: getKukai.ID }
}
