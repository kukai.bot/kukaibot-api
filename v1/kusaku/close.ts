import { query, my, pool } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, changeKusakuStage } from '../../utils'

export default async function (request: STR2OBJ) {
    console.log(request)
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')

    return await changeKusakuStage.code(group, parseInt(request.stage, 10), request.code)
}