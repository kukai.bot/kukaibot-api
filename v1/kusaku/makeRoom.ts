import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, sendNotf, isPremium } from '../../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at, time, set, name, kigo, jidai, gazou, code } = request
    const insertFlag = `${kigo ? '1' : '0'},${jidai ? '1' : '0'},${gazou ? '1' : '0'}`
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const sql = my(`${group}_room`).insert({
        User: i,
        Code: code,
        Name: name,
        Time: time,
        Set: set,
        Odai: insertFlag
    }).toString()
    try {
        await query(sql)
        //await sendNotf(`ルーム${name} が作成されました`, `みんなで句作`, false, null, group, 'NewRoomNotf', null)
        return { success: true }
    } catch (err) {
        console.error(err)
        return errorHandle(`cannot make a room`)
    }
}