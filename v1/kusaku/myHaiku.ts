import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { Kusaku } from '../../interfaces/kusaku'
import { errorHandle, secure } from '../../utils'
import moment from 'moment-timezone'
moment.tz.setDefault("Asia/Tokyo")

export default async function (request: STR2OBJ) {
    const { group, i, at, page, q } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let sql = my(`${group}_kusaku`).select().limit(40 * (page ? parseInt(page, 10) : 1)).where('User', i).orderBy('CreatedAt', 'desc').toString()
    if(q) sql = my(`${group}_kusaku`).select().limit(40 * (page ? parseInt(page, 10) : 1)).where('Haiku', 'like', `%${q}%`).where('User', i).orderBy('CreatedAt', 'desc').toString()
    let result
    try {
        result = await query(sql) as Kusaku[]
    } catch (err) {
        console.error(err)
        return errorHandle(`cannot find haiku`)
    }
    let ret = []
    for (let ku of result) {
        ret.push({
            id: ku.ID,
            text: ku.Haiku,
            createdAt: moment(new Date(ku.CreatedAt)).unix(),
            room: ku.Room,
            solo: ku.Solo ? true : false
        })
    }
    return {
        success: true,
        haiku: ret
    }
}