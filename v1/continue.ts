import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { nameVal, errorHandle, getNameOf, token as genToken } from '../utils'

export default async function (request: STR2OBJ) {
    const { code } = request
    const table = code.split(':')[0]
    let tableName = `${table}_name`
    if (table === 'login') {
        tableName = 'kukaibotLogin'
        let loginTry
        const sqll = my(tableName).select().where('Code', code).toString()
        try {
            loginTry = await query(sqll)
            if (!loginTry[0]['User']) return errorHandle(`Not Found Your ContinueCode in ${table}`)
        } catch (err) {
            return errorHandle(`Not Found Your ContinueCode in ${table}`)
        }
        const id = loginTry[0]['User']
        const provider = id.split(':')[0]
        return { onlyAuth: true, at: id.split(':')[1], provider }
    }
    if (!nameVal(code, 20)) return errorHandle('invalid continueCode')
    const sql = my(tableName).select().where('ContinueCode', code).toString()
    let result
    try {
        result = await query(sql)
        if (!result[0]['ID']) return errorHandle(`Not Found Your ContinueCode in ${table}`)
    } catch (err) {
        return errorHandle(`Not Found Your ContinueCode in ${table}`)
    }
    const name = result[0]['Name']
    const i = result[0]['User']
    const isAdmin = result[0]['Admin']
    const isDev = result[0]['Dev']
    const groupName = await getNameOf.group(table)
    const token = genToken(25)
    const sql2 = my(`${table}_login`).insert({
        User: i,
        Code: token,
        ForContinue: 1
    }).toString()
    await query(sql2)
    return {
        success: true,
        name: groupName,
        i,
        at: token,
        haigo: name,
        prefix: table,
        isAdmin,
        isDev
    }
}