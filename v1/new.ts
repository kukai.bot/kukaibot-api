import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { errorHandle, token as genToken, nameVal, tempLogin } from '../utils'
import axios from 'axios'
import fs from 'fs'
import { CONFIG } from '../interfaces/config'
import * as dotenv from 'dotenv'
dotenv.config()
const config = (process.env as unknown) as CONFIG

export default async function (request: STR2OBJ) {
    const { token, name, prefix, invitation, code, haigo } = request
    if (!nameVal(name, 20)) return errorHandle('cannot use this name')
    const prefixVal = prefix.match(/[a-z0-9]{1,4}/)
    if (prefixVal) {
        if (prefixVal[0] !== prefix) return errorHandle('cannot use this prefix')
    }
    const invitationVal = invitation.match(/[a-zA-Z0-9]{1,20}/)
    if (invitationVal) {
        if (invitationVal[0] !== invitation) return errorHandle('cannot use this invitation code')
    }
    const sqlp = my(`kukaibotKukai`).select().where('Name', prefix).toString()
    try {
        const result = await query(sqlp) as any
        if (result.length) return errorHandle('cannot use this prefix')
    } catch (err) {
        console.log(err)
        return errorHandle('error occured when prefix check')
    }
    let p
    if (token) {
        p = await axios.post(`https://api.pay.jp/v1/charges`, `amount=500&currency=jpy&card=${token}`, {
            auth: {
                username: config.PAYJP_SECRET,
                password: ''
            }
        })
        if (!p.data.paid) return errorHandle(p.data.failure_message)
    }
    const sqls = fs.readFileSync('./init.sql').toString().replace(/%db%/g, config.DB_DATABASE).replace(/%prefix%/g, prefix).split('\n')
    for (const sql of sqls) {
        try {
            await query(sql)
        } catch (err) {
            console.log(err)
            return errorHandle(`cannot initialize the DB`)
        }
    }
    console.log(code)
    const i = await tempLogin(code)
    if (!i) return errorHandle('cannot ulogin')
    const addUser = my(`${prefix}_name`).insert({
        User: i,
        Name: haigo,
        Admin: 1,
        Dev: 0,
        IsLINE: 0,
        Status: ''
    }).toString()
    await query(addUser)
    const sqlpi = my(`${prefix}_config`).insert([{
        Name: 'name',
        Value: name
    },
    {
        Name: 'phrase',
        Value: invitation
    },
    {
        Name: 'premium',
        Value: token && p?.data.paid ? 1 : 0
    }]).toString()
    try {
        const result = await query(sqlpi) as any
        if (result.length) return errorHandle('cannot use this prefix')
    } catch (err) {
        console.log(err)
        return errorHandle('error occured when prefix check')
    }
    const sqlc = my(`kukaibotKukai`).insert({
        Name: prefix,
        Owner: i
    }).toString()
    try {
        const result = await query(sqlc) as any
        if (result.length) return errorHandle('cannot use this prefix')
    } catch (err) {
        console.log(err)
        return errorHandle('error occured when prefix check')
    }
    return { success: true }
}