import { query, my, pool } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { Kukai } from '../interfaces/kukai'
import { isAdmin, isDev, errorHandle, kukaiCount, secure } from '../utils'
import moment from 'moment-timezone'
moment.tz.setDefault("Asia/Tokyo")

export default async function (request: STR2OBJ) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let limit = 40
    const limitParam = parseInt(request.limit ? request.limit : '0', 10)
    if (limitParam <= 40 && limitParam >= 1) limit = limitParam
    const sql = my(`${group}_kukai`).select().where('IsKusaku', 0).limit(limit).orderBy('CreatedUTC', 'desc').toString()
    let result: Kukai[]
    try {
        result = await query(sql)
    } catch (err) {
        return errorHandle(`cannot get any kukai`)
    }
    let admin = false
    const ret = []
    const dev = await isDev(group, i)
    const db = await pool()
    if (await isAdmin(group, i)) admin = true
    for (const kukai of result) {
        const name = kukai['Name']
        if (name.match(/test:.+/) && !dev) continue
        let write = false
        let read = false
        let deletable = false
        let onsen = false
        let onsenR = false
        let myKukai = false
        let deadline: null | string = null
        if (kukai['PDF'] === 0) write = true
        if (kukai['PDF'] === 3) read = true
        if (kukai['OnSen'] === 1) onsen = true
        if (kukai['OnSenResult'] === 1) onsenR = true
        if (kukai['User'] === i) {
            myKukai = true
            deletable = true
        }
        if (admin) deletable = true
        const comment = kukai['OnSenComment']
        if (moment(kukai['Deadline']).isValid()) deadline = new Date(kukai['Deadline']).toISOString()
        const id = kukai['ID']
        const ct = await kukaiCount.getKukaiCount(id, group, db)
        const ctYou = await kukaiCount.getKukaiCountByUser(id, i, group, db)
        let tkct = '投句なし'
        if (ct > 50) {
            tkct = `投句数:${ctYou} (全体:50~)`
        } else if (ct > 30) {
            tkct = `投句数:${ctYou} (全体:30~49)`
        } else if (ct > 10) {
            tkct = `投句数:${ctYou} (全体:10~29)`
        } else if (ct > 0) {
            tkct = `投句数:${ctYou} (全体:数句)`
        }
        const json = {
            name,
            deadline,
            createdAt: kukai['CreatedUTC'],
            id: kukai['ID'],
            read,
            write,
            isMine: myKukai,
            deletable,
            onsen,
            onsenResult: onsenR,
            count: tkct
        }
        ret.push(json)
    }
    db.end()
    return {
        success: true,
        kukai: ret
    }
}