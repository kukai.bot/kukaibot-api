import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, kukaiCharge, isAdmin } from '../../utils'

export default async function (request: STR2OBJ, id: number) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if(!await isAdmin(group, i) && await kukaiCharge(group, id) !== i) return errorHandle('you are neither owner nor admin')
    const sql = my(`${group}_kukai`).select().where('ID', id).toString()
    try {
        const result = await query(sql)
        if(!result) return errorHandle(`cannot find kukai`)
        if(result.length !== 1) return errorHandle(`cannot find kukai`)
    } catch (err) {
        return errorHandle(`cannot find kukai`)
    }
    const sql2 = my(`${group}_kukai`).delete().where('ID', id).toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot delete kukai`)
    }
}