import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure } from '../../utils'

export default async function (request: STR2OBJ, id: number) {
    const { group, i, at, delete: del, haiku, code } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if (del) {
        const sql = my(`${group}_haiku`).delete().where('Code', code).where('Kukai', id).where('User', i).toString()
        try {
            await query(sql)
            return { success: true }
        } catch (err) {
            return errorHandle(`cannot delet this haiku`)
        }
    } else {
        const sql = my(`${group}_haiku`).update({
            Haiku: haiku
        }).where('Code', code).where('User', i).where('Kukai', id).toString()
        try {
            await query(sql)
            return { success: true }
        } catch (err) {
            return errorHandle(`cannot delet this haiku`)
        }
    }
}