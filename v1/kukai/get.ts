import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure } from '../../utils'

export default async function (request: STR2OBJ, id: number, onlyMine: boolean) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    let sql = my(`${group}_haiku`).select().where('Kukai', id).orderBy('Code', 'desc').toString()
    if(onlyMine) {
        sql = my(`${group}_haiku`).select().where('Kukai', id).where('User', i).toString()
    } else {
        const sql2 = my(`${group}_kukai`).select('PDF').where('ID', id).toString()
        try {
            const pdf = await query(sql2)
            if(parseInt(pdf[0]['PDF'], 10) !== 3) return errorHandle('you can do it only if the kukai has been closed.')
        } catch (err) {
            return errorHandle(`cannot find kukai`)
        }
    }
    let result
    try {
        result = await query(sql)
    } catch (err) {
        return errorHandle(`cannot find kukai`)
    }
    let ret = []
    for (let ku of result) {
        ret.push({
            code: ku['Code'],
            text: ku['Haiku']
        })
    }
    return {
        success: true,
        haiku: ret
    }
}