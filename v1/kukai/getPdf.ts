import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, isAdmin, kukaiCharge, genPdfUrl } from '../../utils'

export default async function (request: STR2OBJ, id: number) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const sql2 = my(`${group}_kukai`).select('PDF').where('ID', id).toString()
    try {
        const pdf = await query(sql2)
        if (parseInt(pdf[0]['PDF'], 10) !== 3) return errorHandle('you can do it only if the kukai has been closed.')
    } catch (err) {
        return errorHandle(`cannot find kukai`)
    }
    let revealed = false
    let revUrl = null
    if(await isAdmin(group, i) || await kukaiCharge(group, id) === i) {
        revealed = true
        revUrl = await genPdfUrl(id, true, group)
    }
    const url = await genPdfUrl(id, false, group)
    return {
        revealed: revealed,
        revealed_url: revUrl,
        url: url
    }
}