import { errorHandle, closeKukaiCore, secure } from '../../utils'
import { STR2OBJ } from '../../interfaces/common'

export default async function (request: STR2OBJ, id: number) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    await closeKukaiCore.core(id, group)
    return { success: true }
}