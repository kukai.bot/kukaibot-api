import { query, my } from '../../mysql'
import { POSTKUKAI } from '../../interfaces/common'
import { errorHandle, secure, token as genToken, nameVal } from '../../utils'

export default async function (request: POSTKUKAI, id: number) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const sql = my(`${group}_kukai`).select().where('ID', id).toString()
    try {
        const result = await query(sql)
        if(!result) return errorHandle(`cannot find kukai`)
        if(result[0]['PDF'] !== 0) return errorHandle(`cannot post to this kukai`)
        if(result.length !== 1) return errorHandle(`cannot find kukai`)
    } catch (err) {
        return errorHandle(`cannot find kukai`)
    }
    const ku = request.ku as string[]
    let error = false
    let errorMeg = ''
    for(let haiku of ku) {
        const code = genToken(5)
        const sql2 = my(`${group}_haiku`).insert({
            User: i,
            Haiku: haiku,
            Kukai: id,
            Code: code
        }).toString()
        if(!nameVal(haiku, 34)) {
            error = true
            errorMeg = `this haiku ${haiku} is not valid(maybe too long)`
        } else {
            try {
                await query(sql2)
                continue
            } catch (err) {
                break
            }
        }
    }
    if(!error) return { success: true }
    return errorHandle(errorMeg)
}