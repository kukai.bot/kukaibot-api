import { query, my, pool } from '../mysql'

import { Kukai } from '../interfaces/kukai'
import moment from 'moment-timezone'
moment.tz.setDefault("Asia/Tokyo")
import { closeKukaiCore, sendNotf } from '../utils'

export default async () => {
    const db = await pool()
    const sql = my(`kukaibotKukai`).select('Name').toString()
    let groups: any
    try {
        [groups] = await db.query(sql) as any
    } catch (err) {
        return false
    }
    for (const groupRaw of groups) {
        const group = groupRaw['Name']
        const sqlf = my(`${group}_kukai`).select().where('PDF', 0).whereNotNull('Deadline').toString()
        let result: Kukai[]
        try {
            [result] = await db.query(sqlf) as any
        } catch (err) {
            return false
        }
        const now = Math.floor(new Date().getTime() / 1000)
        for (const kukai of result) {
            const deadline = Math.floor(new Date(kukai['Deadline']).getTime() / 1000)
            if (now > deadline) {
                // 現在rnの締め切りはLINE側が行っています
                if (group === 'rn_open') {
                    await sendNotf(`句会${kukai['Name']} が締め切られました`, `締切`,  false, null, group, 'KukaiEndNotf', null)
                } else {
                    await closeKukaiCore.core(kukai['ID'], group)
                }
            }
            const tenMinsBefore = deadline - 600
            if (now > tenMinsBefore && now < tenMinsBefore + 60) {
                // Notification
                await sendNotf(`句会${kukai['Name']} の締切まであと10分です。あなたはこの句会に%d句投句しています。`, `締切まであと10分`, true, db, group, 'TenMinsNotf', kukai.ID)
            }
            const oneDayBefore = deadline - 60 * 60 * 24
            if (now > oneDayBefore && now < oneDayBefore + 60) {
                // Notification
                await sendNotf(`句会${kukai['Name']} の締切まであと24時間です。あなたはこの句会に%d句投句しています。`, `締切まであと24時間`, true, db, group, 'OneDayNotf', kukai.ID)
            }
        }
    }

    db.end()
    return { success: true, requested: moment().unix() }
}
