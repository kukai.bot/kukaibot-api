import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { MEMBER } from '../interfaces/member'
import { errorHandle, secure, getProfile, isAdmin } from '../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')

    const sql = my(`${group}_name`).select().toString()
    let result: MEMBER[]
    try {
        result = await query(sql)
    } catch (err) {
        return errorHandle(`cannot find any member`)
    }
    const iAmAdmin = await isAdmin(group, i) ? true : false
    const ret = []
    for(const member of result) {
        const name = member['Name']
        const id = member['User']
        const adminStr = member['Admin']
        let admin = false
        if(adminStr) admin = true
        let profile = {
            displayName: null,
            pictureUrl: null
        }
        if(member['IsLINE']) {
            const lineProfile = await getProfile.line(id)
            if(lineProfile) {
                profile = {
                    displayName: lineProfile.displayName,
                    pictureUrl: lineProfile.pictureUrl
                }
            }
        }
        ret.push({
            name,
            userId: id,
            admin,
            profile
        })
    }
    return {
        admin: iAmAdmin,
        members: ret
    }
}