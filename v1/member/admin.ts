import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, isAdmin } from '../../utils'

export default async function (request: STR2OBJ, id: string, action: string) {
    const { group, i, at } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if(!await isAdmin(group, i)) return errorHandle('you are not admin')
    const sql = my(`${group}_name`).select().where('User', id).toString()
    try {
        const result = await query(sql)
        if(!result) return errorHandle(`cannot find member`)
        if(result.length !== 1) return errorHandle(`cannot find member`)
    } catch (err) {
        return errorHandle(`cannot find member`)
    }
    let act = 0
    if(action === 'add') {
        act = 1
    }
    const sql2 = my(`${group}_name`).update({
        Admin: act
    }).where('User', id).toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot process the action to member`)
    }
}