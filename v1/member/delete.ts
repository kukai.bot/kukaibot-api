import { query, my } from '../../mysql'
import { STR2OBJ } from '../../interfaces/common'
import { errorHandle, secure, isAdmin } from '../../utils'

export default async function (request: STR2OBJ, id: string) {
    const { group, i, at, quitGroup } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if(!await isAdmin(group, i) && !quitGroup) return errorHandle('you are not admin')
    if(quitGroup && id !== i) return errorHandle('this action is over your power')
    const sql = my(`${group}_name`).select().where('User', id).toString()
    try {
        const result = await query(sql)
        if(!result) return errorHandle(`cannot find member`)
        if(result.length !== 1) return errorHandle(`cannot find member`)
    } catch (err) {
        return errorHandle(`cannot find member`)
    }
    const sql2 = my(`${group}_name`).delete().where('User', id).toString()
    try {
        await query(sql2)
    } catch (err) {
        return errorHandle(`cannot delete member`)
    }
    const sql3 = my(`${group}_haiku`).delete().where('User', id).toString()
    try {
        await query(sql3)
    } catch (err) {
        return errorHandle(`cannot delete member's haiku`)
    }
    const sql4 = my(`${group}_notf`).delete().where('User', id).toString()
    try {
        await query(sql4)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot delete member's notification data`)
    }
}