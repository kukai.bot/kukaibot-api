import { query, my } from '../../mysql'
import { errorHandle, secure, getComment, checkOnSen } from '../../utils'

export default async function (request: any) {
    const { group, token, id } = request
    const i = await secure.securityCheckOnline(group, token)
    if (!i) return errorHandle('cannot pass security check for online')
    const status = await checkOnSen(group, parseInt(id))
    if (status !== 1) return errorHandle('not started yet, already closed or something error')
    const sql2 = my(`${group}_haiku`).select().where('Kukai', id).orderBy('Code', 'desc').toString()
    try {
        const kukais = await query(sql2)
        let ret = []
        for (let kukai of kukais) {
            ret.push({
                code: kukai['Code'],
                ku: kukai['Haiku'],
                show: false,
                comment: null,
                point: 0
            })
        }
        return {success: true, comment: await getComment(group, id), json: ret}
    } catch (err) {
        return errorHandle(`cannot create OnSen`)
    }
}