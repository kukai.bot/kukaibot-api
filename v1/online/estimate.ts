import { query, my } from '../../mysql'
import { errorHandle, secure, isAdmin, kukaiCharge, checkOnSen } from '../../utils'
interface Sen {
    code: string
    point: number
    comment: string
}
export default async function (request: any) {
    const { group, token, id, json } = request
    const i = await secure.securityCheckOnline(group, token)
    if (!i) return errorHandle('cannot pass security check for online')
    if (!await isAdmin(group, i) && !await kukaiCharge(group, parseInt(id)) === i) return errorHandle('you cannot complete this action')
    const status = await checkOnSen(group, parseInt(id))
    if (status !== 1) return errorHandle('not started yet or something error')
    let obj = []
    for (let ku of json as Sen[]) {
        obj.push({
            Kukai: id,
            Haiku: ku.code,
            Point: ku.point,
            Comment: ku.comment,
            Creator: i
        })
    }
    const sql2 = my(`${group}_sen`).insert(obj).toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot create OnSen`)
    }
}