import { query, my } from '../../mysql'
import { errorHandle, secure, getComment, checkOnSen, getProfile, isOnSenZero } from '../../utils'

export default async function (request: any) {
    const { group, token, id } = request
    const i = await secure.securityCheckOnline(group, token)
    if (!i) return errorHandle('cannot pass security check for online')
    const status = await checkOnSen(group, parseInt(id))
    if (status !== 2) return errorHandle('not started yet, already closed or something error')
    const sql = my(`${group}_haiku`).select().where('Kukai', id).orderBy('Code', 'desc').toString()
    const zero = await isOnSenZero(group, id)
    try {
        const kukais = await query(sql)
        let ret = []
        let cr: string[] = []
        for (let kukai of kukais) {
            const senR = await sen(group, kukai['Code'])
            const user = kukai['User']
            const prof = await getProfile.idToName(group, user)
            cr = cr.concat(senR.creator)
            ret.push({
                code: kukai['Code'],
                ku: kukai['Haiku'],
                comments: senR.result,
                pointer: senR.pointer,
                show: false,
                point: senR.point,
                creator: zero && !senR.point ? null : user,
                creatorName: zero && !senR.point ? null : prof,
            })
        }
        const retRaw = JSON.parse(JSON.stringify(ret))
        ret.sort(function (a, b) {
            if (a.point < b.point) return 1
            if (a.point > b.point) return -1
            return 0;
        })
        const filteredCr = cr.filter(function (x, i, self) {
            return self.indexOf(x) === i
        })
        let people = []
        for (let cra of filteredCr) {
            people.push({
                id: cra,
                name: await getProfile.idToName(group, cra)
            })
        }
        return {
            success: true,
            default: retRaw,
            sorted: ret,
            people: people,
            comment: await getComment(group, id)
        }
    } catch (err) {
        return errorHandle(`cannot create OnSen`)
    }
}
interface Pointer {
    id: string
    name: string
}
async function sen(group: string, code: string) {
    const sql = my(`${group}_sen`).select().where('Haiku', code).toString()
    try {
        const sens = await query(sql)
        let cr = []
        let ret = []
        let pt = 0
        let pointer = [[], [], [], []] as [Pointer[], Pointer[], Pointer[], Pointer[]]
        for (let sen of sens) {
            ret.push({
                comment: sen.Comment,
                creator: sen.Creator,
                creatorName: await getProfile.idToName(group, sen.Creator),
                point: sen.Point
            })
            pointer[sen.Point === 0.5 ? 3 : sen.Point].push({
                id: sen.Creator,
                name: await getProfile.idToName(group, sen.Creator)
            })
            cr.push(sen.Creator)
            pt = parseInt(sen.Point) + pt
        }
        return {
            result: ret,
            pointer: pointer,
            point: pt,
            creator: cr
        }
    } catch (err) {
        return {
            result: [],
            point: 0,
            creator: []
        }
    }
}