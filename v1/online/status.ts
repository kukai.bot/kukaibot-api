import { errorHandle, secure, isAdmin, kukaiCharge, checkOnSen, getComment, isOnSenZero } from '../../utils'

export default async function (request: any) {
    const { group, token, id } = request
    const i = await secure.securityCheckOnline(group, token)
    if (!i) return errorHandle('cannot pass security check for online')
    let admin = true
    if (!await isAdmin(group, i) && await kukaiCharge(group, parseInt(id)) !== i) admin = false
    const status = await checkOnSen(group, parseInt(id))
    return {
        success: true,
        status: status,
        admin: admin,
        comment: await getComment(group, id),
        zero: await isOnSenZero(group, id)
    }
}