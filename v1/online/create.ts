import { query, my } from '../../mysql'
import { errorHandle, secure, isAdmin, kukaiCharge, checkOnSen } from '../../utils'

export default async function (request: any) {
    const { group, token, comment, zero, id } = request
    const i = await secure.securityCheckOnline(group, token)
    if (!i) return errorHandle('cannot pass security check for online')
    if (!await isAdmin(group, i) && !await kukaiCharge(group, parseInt(id)) === i) return errorHandle('you cannot complete this action')
    const sql2 = my(`${group}_kukai`).update({
        OnSenComment: comment,
        OnSen: 1,
        OnSenZero: zero ? 1 : 0
    }).where('ID', id).toString()
    try {
        await query(sql2)
        return { success: true }
    } catch (err) {
        console.log(err)
        return errorHandle(`cannot create OnSen`)
    }
}