import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { errorHandle, token as genToken, secure, checkOnSen, isAdmin, kukaiCharge } from '../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at, id, isKusaku } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    const code = genToken(30)
    let admin = true
    if (!await isAdmin(group, i) && await kukaiCharge(group, parseInt(id, 10)) !== i) admin = false
    const status = await checkOnSen(group, parseInt(id, 10))
    if(!isKusaku) if(!admin && status < 0) return errorHandle('管理者またはこの句会の作成者がオンにするまで利用できません')
    const sql = my(`${group}_login`).insert({
        User: i,
        Code: code
    }).toString()
    try {
        await query(sql)
        return { code }
    } catch (err) {
        return errorHandle(`cannot connect`)
    }
}