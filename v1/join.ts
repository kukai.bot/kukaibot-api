import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { nameVal, errorHandle, getInvitationCode, getNameOf, secure, token as genToken, isPremium } from '../utils'

async function v1Join(request: STR2OBJ) {
    const { name, i, id, pwd } = request
    if (!nameVal(name, 20)) return errorHandle('cannot use this name')
    const code = await getInvitationCode(id)
    const groupName = await getNameOf.group(id)
    if (pwd !== code) return errorHandle('unauthorized')
    let isAdmin = false
    let isDev = false
    if (await secure.accesibleApi(request, id)) {
        const sqlN = my(`${id}_name`).select().where('User', i).toString()
        try {
            const result = await query(sqlN)
            isAdmin = result[0]['Admin']
            isDev = result[0]['Dev']
        } catch (err) {
            return errorHandle('cannot process to check the user')
        }
    } else {
        const sqlC = my(`${id}_name`).select().where('User', i).toString()
        try {
            const result = await query(sqlC)
            if(result.length >= 20 && await isPremium(id)) return errorHandle('非プレミアムグループでは20人までしか登録できません')
        } catch (err) {
            return errorHandle('cannot process to check how many users in this group')
        }
        const sqlT = my(`${id}_name`).insert({
            User: i,
            Name: name,
            Status: 'native'
        }).toString()
        try {
            await query(sqlT)
        } catch (err) {
            return errorHandle('cannot process to insert the user')
        }
    }
    const token = genToken(25)
    const sql = my(`${id}_login`).insert({
        User: i,
        Code: token
    }).toString()
    try {
        await query(sql)
    } catch (err) {
        return false
    }
    return {
        success: true,
        isAdmin,
        isDev,
        at: token,
        name: groupName
    }
}
export default v1Join