import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { MEMBER } from '../interfaces/member'
import { isAdmin, getInvitationCode, errorHandle, secure, tempLogin, getNameOf, isPremium } from '../utils'

export default async function (request: STR2OBJ) {
    let i: string | null
    let notificationId: string | null
    const { group } = request
    if (request.authMethod === 'code') {
        const { code } = request
        const tl = await tempLogin(code)
        i = null
        if (typeof tl === 'string') i = tl
        if (!i) return errorHandle('cannot get via login code')
        notificationId = null
    } else {
        const { at } = request
        i = request.i
        notificationId = request.notificationId
        if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    }
    const sql = my(`${group}_name`).select().where('User', i).toString()
    let result: [MEMBER]
    try {
        result = await query(sql)
    } catch (err) {
        return errorHandle(`cannot get any user data`)
    }
    let nid = '0'
    if (notificationId) nid = notificationId
    const sqlN = my(`${group}_notf`).select().where('User', i).where('Token', nid).toString()
    let resultN
    try {
        resultN = await query(sqlN)
    } catch (err) {
        return errorHandle(`cannot get any notf data`)
    }
    const odn = resultN[0] ? resultN[0].OneDayNotf : false
    const tmn = resultN[0] ? resultN[0].TenMinsNotf : false
    const nkn = resultN[0] ? resultN[0].NewKukaiNotf : false
    const ken = resultN[0] ? resultN[0].KukaiEndNotf : false

    return {
        admin: await isAdmin(group, i) ? true : false,
        haigo: result[0].Name,
        name: await getNameOf.group(group),
        premium: await isPremium(group),
        invitationCode: await getInvitationCode(group),
        notificationId: resultN[0] ? resultN[0].Token : null,
        notification: {
            oneDayBefore: odn ? true : false,
            tenMinsBefore: tmn ? true : false,
            newKukaiNotf: nkn ? true : false,
            kukaiEndNotf: ken ? true : false
        }
    }
}