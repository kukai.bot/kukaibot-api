import { query, my } from '../mysql'
import { STR2OBJ } from '../interfaces/common'
import { errorHandle, secure, sendNotf, isPremium } from '../utils'

export default async function (request: STR2OBJ) {
    const { group, i, at, name } = request
    if (!await secure.securityCheck(group, i, at)) return errorHandle('cannot pass security check')
    if(request.deadline) {
        if(!await isPremium(group)) return errorHandle('締め切り指定はプレミアム限定のサービスです')
    }
    const sql = my(`${group}_kukai`).insert({
        User: i,
        Name: name,
        PDF: 0,
        Deadline: request.deadline ? request.deadline : null
    }).toString()
    try {
        await query(sql)
        await sendNotf(`句会${name} が作成されました`, `新しい句会`,  false, null, group, 'NewKukaiNotf', null)
        return { success: true }
    } catch (err) {
        return errorHandle(`cannot make kukai`)
    }
}