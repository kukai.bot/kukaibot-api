import Router from 'koa-router'
const router = new Router()
import 'koa-body' // 型定義が一部@types/koaとkoa-bodyで競合する???ので
import * as api from './kusakuApi'

router.get('/v1/kusaku/odai', async (ctx, next) => {
    const body = await api.v1Odai(ctx.request.query)
    ctx.body = body
})
router.post('/v1/kusaku/solo_post', async (ctx, next) => {
    const body = await api.v1SoloPost(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/post', async (ctx, next) => {
    const body = await api.v1Post(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/room/make', async (ctx, next) => {
    const body = await api.v1MakeRoom(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/room/list', async (ctx, next) => {
    const body = await api.v1RoomList(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/room/close', async (ctx, next) => {
    const body = await api.v1Close(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/room/go_sen', async (ctx, next) => {
    const body = await api.v1GoSen(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/my/status', async (ctx, next) => {
    const body = await api.v1Status.myStatus(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/all/status', async (ctx, next) => {
    const body = await api.v1Status.allStatus(ctx.request.body)
    ctx.body = body
})
router.post('/v1/kusaku/my', async (ctx, next) => {
    const body = await api.v1MyHaiku(ctx.request.body)
    ctx.body = body
})
export default router