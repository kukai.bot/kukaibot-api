import o from './v1/kusaku/odai'
export const v1Odai = o
import sp from './v1/kusaku/soloPost'
export const v1SoloPost = sp
import p from './v1/kusaku/post'
export const v1Post = p
import r from './v1/kusaku/makeRoom'
export const v1MakeRoom = r
import l from './v1/kusaku/roomList'
export const v1RoomList = l
import c from './v1/kusaku/close'
export const v1Close = c
import g from './v1/kusaku/goSen'
export const v1GoSen = g
import * as s from './v1/kusaku/status'
export const v1Status = s
import m from './v1/kusaku/myHaiku'
export const v1MyHaiku = m