import ws from 'websocket'
const WebSocketServer = ws.server
import http from 'http'
import url from 'url'
import { query, my, pool } from '../mysql'
import { secure, getProfile } from '../utils'
import { STR2OBJ } from '../interfaces/common'
const server = http.createServer(function (request, response) {
    console.log((new Date()) + ' Received request for ' + request.url)
    response.writeHead(404)
    response.end()
})
server.listen(8005, function () {
    console.log((new Date()) + ' Server is listening on port 8005')
})
const wss = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
})
//Websocket接続を保存しておく
let connections: { ws: ws.connection, target: string }[] = []
let ct = {} as any
//接続時
wss.on('request', async function (req) {
    if (!req.httpRequest.url) return false
    const query = url.parse(req.httpRequest.url, true).query
    // Accept the request.
    const { group, i, at } = query as STR2OBJ
    if (!await secure.securityCheck(group, i, at)) return false
    if (typeof query.target !== 'string') return false
    const { target } = query
    const ws = req.accept()
    connections.push({ ws, target })
    typeof ct[target] === 'number' ? ct[target]++ : ct[target] = 1
    const name = await getProfile.idToName(group, i)
    const send = `${name}が参加しました`
    broadcast(target, JSON.stringify({ target, count: ct[target], text: send, type: 'addUser' }))
    //切断時
    ws.on('close', function () {
        ct[target]--
        connections = connections.filter(function (conn, i) {
            return (conn.ws === ws) ? false : true
        })
        let data = null
        if (query.owner) data = 'close'
        console.log(query.owner, data)
        broadcast(target, JSON.stringify({ target, count: ct[target], text: `${name}が退出しました`, data, type: 'removeUser' }))
    })
    //メッセージ送信時
    ws.on('message', async function (message) {
        const mes = JSON.parse(message.utf8Data || '[]')
        let data = mes.data ? mes.data : null
        let { content } = mes
        if (mes.type === 'kusaku') {
            content = `${name}が投句しました`
        }
        broadcast(target, JSON.stringify({ target, text: content, data, type: mes.type }))
    })
})
//ブロードキャストを行う
function broadcast(target: string, message: string) {
    connections.forEach(function (con, i) {
        if (con.target === target) con.ws.send(message)
    })
}