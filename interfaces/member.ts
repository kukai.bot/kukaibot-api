export interface MEMBER {
    ID: number
    User: string
    Name: string
    Admin: 0 | 1
    Dev: 0 | 1
    Kukai: number
    Status: string
    IsLINE: 0 | 1
    ContinueCode: string
    ExpoNotification: string
    OneDayNotf: 0 | 1
    TenMinsNotf: 0 | 1
    NewKukaiNotf: 0 | 1
    KukaiEndNotf: 0 | 1
}