export interface CONFIG {
    DB_HOST : string
    DB_USER: string
    DB_PASSWORD: string
    DB_DATABASE: string
    PEM_AT: string
    KEYPAIR_ID: string
    LINE_AUTH: string
    GOOGLE_CLIENTID: string
    GOOGLE_CLIENTSECRET: string
    HOST: string
    PAYJP_SECRET: string
    PAYJP_PUBLIC: string
    PDF_HODT: string
    PDF_PASSWORD: string
}