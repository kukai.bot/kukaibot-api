export interface Kukai {
    ID: number
    CreatedUTC: string
    User: string
    Name: string
    Deadline: string
    PDF: number
    Notice: 0 | 1
    OnSen: 0 | 1
    OnSenResult: 0 | 1
    OnSenComment: string
    OnSenZero: 0
}