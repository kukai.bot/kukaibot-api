export interface RoomNew {
    Code: string
    User: string
    Name: string
    Time: number
    Set: number
    Odai: string
}
export interface Room extends RoomNew {
    ID: string
    NowSet: number
    CreatedAt: string
    Stage: number // 1: Opening, 2: Sen available, 3: Closed without sen, 4: Sen closed
    Member: number
    Kukai: number
}
export interface KusakuNew {
    User: string
    Haiku: string
    Room: string
    Solo: number
}
export interface Kusaku extends KusakuNew {
    ID: number
    CreatedAt: string
}