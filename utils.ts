import n from './utils/nameVal'
export const nameVal = n
import e from './utils/errorHandle'
export const errorHandle = e
import gi from './utils/getInvitationCode'
export const getInvitationCode = gi
import * as gn from './utils/getNameOf'
export const getNameOf = gn
import * as s from './utils/secure'
export const secure = s
import t from './utils/token'
export const token = t
import {admin, dev} from './utils/isAdmin'
export const isAdmin = admin
export const isDev = dev
import * as gK from './utils/kukaiCount'
export const kukaiCount = gK
import c from './utils/kukaiCharge'
export const kukaiCharge = c
import * as ckc from './utils/closeKukaiCore'
export const closeKukaiCore = ckc
import g from './utils/genPdfUrl'
export const genPdfUrl = g
import * as gl from './utils/getProfile'
export const getProfile = gl
import p from './utils/sendNotf'
export const sendNotf = p
import ip from './utils/isPremium'
export const isPremium = ip
import tl from './utils/tempLogin'
export const tempLogin = tl
import cos from './utils/checkOnSen'
export const checkOnSen = cos
import gc from './utils/getComment'
export const getComment = gc
import io from './utils/isOnSenZero'
export const isOnSenZero = io
import * as cs from './utils/changeKusakuStage'
export const changeKusakuStage = cs
