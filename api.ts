import apiV1Join from './v1/join'
export const v1Join = apiV1Join
import apiV1Continue from './v1/continue'
export const v1Continue = apiV1Continue
import apiKukais from './v1/kukais'
export const v1Kukais = apiKukais
import mk from './v1/makeKukai'
export const v1MakeKukai = mk
import dk from './v1/kukai/delete'
export const v1DeleteKukai = dk
import ck from './v1/kukai/close'
export const v1CloseKukai = ck
import pk from './v1/kukai/post'
export const v1PostKukai = pk
import mh from './v1/kukai/get'
export const v1GetHaiku = mh
import eh from './v1/kukai/edit'
export const v1EditHaiku = eh
import gp from './v1/kukai/getPdf'
export const v1GetPDF = gp
import m from './v1/members'
export const v1Members = m
import md from './v1/member/delete'
export const v1DeleteMember = md
import a from './v1/member/admin'
export const v1AdminControl = a
import s from './v1/settings'
export const v1Settings = s
import iu from './v1/setting/invitationCode'
export const v1InvitCodeUpdate = iu
import n from './v1/setting/notf'
export const v1NotifUpdate = n
import h from './v1/setting/haigo'
export const v1HaigoUpdate = h
import w from './v1/setting/web'
export const v1WebSetting = w
import c from './v1/cron'
export const v1Cron = c
import cn from './v1/connect'
export const v1Connect = cn
import t from './v1/new'
export const v1New = t
import oc from './v1/online/create'
export const v1OnlineCreate = oc
import os from './v1/online/status'
export const v1OnlineStatus = os
import ol from './v1/online/close'
export const v1OnlineClose = ol
import oe from './v1/online/estimate'
export const v1OnlineEstimate = oe
import or from './v1/online/result'
export const v1OnlineResult = or
import ot from './v1/online/select'
export const v1OnlineSelect = ot