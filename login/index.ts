import * as g from './google'
export const google = g
import * as a from './apple'
export const apple = a
import { query, my } from '../mysql'
import { errorHandle, token as genToken } from '../utils'

export async function writeId(provider: string, id: string) {
    const token = `login:${genToken(6)}`
    const sql = my(`kukaibotLogin`).insert({
        User: `${provider}:${id}`,
        Code: token
    }).toString()
    try {
        await query(sql)
        return token
    } catch (err) {
        return false
    }
}