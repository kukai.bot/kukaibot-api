
import querystring from 'querystring'
import { CONFIG } from '../interfaces/config'
import * as dotenv from 'dotenv'
import axios from 'axios'
dotenv.config()
const config = (process.env as unknown) as CONFIG
export function loginRedirect(from: string) {
    const q = querystring.stringify({
        client_id: config.GOOGLE_CLIENTID,
        redirect_uri: `https://${config.HOST}/login/google/redirect`,
        scope: 'https://www.googleapis.com/auth/userinfo.profile',
        response_type: 'code',
        state: from
    })
    const url = `https://accounts.google.com/o/oauth2/auth?${q}`
    return url
}
export async function getToken(code: string) {
    const login = await axios.post('https://accounts.google.com/o/oauth2/token', {
        code: code,
        client_id: config.GOOGLE_CLIENTID,
        client_secret: config.GOOGLE_CLIENTSECRET,
        redirect_uri: `https://${config.HOST}/login/google/redirect`,
        grant_type: 'authorization_code'
    })
    return login.data.access_token
}