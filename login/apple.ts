import jwt from 'jsonwebtoken'

export async function getToken(data: string) {
    const decoded = jwt.decode(data)
    return decoded ? decoded.sub : false
}